package view;

import exception.IpFormatException;
import exception.SikeThatsTheWrongNumberException;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import util.NetworkCalculation;

public class MachineNetworkVB extends VBox {
	private Text txtTitle;
	private GridPane gpInputs;
	private Label lblIPMachine;
	private TextField txfIPMachine;
	private Label lblMask;
	private TextField txfMask;
	private Label lblIPNetwork;
	private TextField txfIPNetwork;
	private Button btnReturn;
	private Button btnClear;
	private Button btnValidate;
	private Text txtAnswer;

	public MachineNetworkVB() {
		//HBox pour les boutons
		HBox hbButtons = new HBox(getBtnReturn(), getBtnClear(), getBtnValidate());
		hbButtons.setAlignment(Pos.CENTER);
		hbButtons.setSpacing(IGraphicConstants.NORMAL_SPACING);

		this.getChildren().addAll(getTxtTitle(), getGpInputs(), getTxtAnswer(), hbButtons);
		this.setSpacing(IGraphicConstants.NORMAL_SPACING);
		this.setAlignment(Pos.CENTER);
	}

	public Text getTxtTitle() {
		if(txtTitle == null) {
			txtTitle = new Text("Machine appartenant au reseau");
			txtTitle.getStyleClass().add("txt-title");
		}
		return txtTitle;
	}

	public GridPane getGpInputs() {
		if(gpInputs == null) {
			gpInputs = new GridPane();
			//Defining the spacings and padding
			gpInputs.setHgap(IGraphicConstants.NORMAL_SPACING);
			gpInputs.setVgap(IGraphicConstants.NORMAL_SPACING);
			gpInputs.setPadding(new Insets(IGraphicConstants.NORMAL_PADDING));
			//Defining the width of the gridpane
			int nbCols = 2;
			for(int i=0; i<nbCols; i++) {
				ColumnConstraints constraint = new ColumnConstraints();
				constraint.setPercentWidth(100./nbCols);
				gpInputs.getColumnConstraints().add(constraint);
			}
			//Adding the elements to the gridpane
			gpInputs.add(getLblIPMachine(), 0, 0);
			gpInputs.add(getTxfIPMachine(), 1, 0);
			gpInputs.add(getLblIPNetwork(), 0, 1);
			gpInputs.add(getTxfIPNetwork(), 1, 1);
			gpInputs.add(getLblMask(), 0, 2);
			gpInputs.add(getTxfMask(), 1, 2);
			//Aligning the elements in the gridpane
			for(Node node : gpInputs.getChildren()) {
				if(node instanceof Label) {
					GridPane.setHalignment(node, HPos.RIGHT);
				}
			}
		}
		return gpInputs;
	}

	public Label getLblIPMachine() {
		if(lblIPMachine == null) {
			lblIPMachine = new Label("Adresse IP de la machine : ");
		}
		return lblIPMachine;
	}

	public TextField getTxfIPMachine() {
		if(txfIPMachine == null) {
			txfIPMachine = new TextField();
			txfIPMachine.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.DOWN) {
					getTxfIPNetwork().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfIPMachine;
	}

	public Label getLblMask() {
		if(lblMask == null) {
			lblMask = new Label("Masque du reseau : ");
		}
		return lblMask;
	}

	public TextField getTxfMask() {
		if(txfMask == null) {
			txfMask = new TextField();
			txfMask.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.UP) {
					getTxfIPNetwork().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfMask;
	}

	public Label getLblIPNetwork() {
		if(lblIPNetwork == null) {
			lblIPNetwork = new Label("Adresse reseau : ");
		}
		return lblIPNetwork;
	}

	public TextField getTxfIPNetwork() {
		if(txfIPNetwork == null) {
			txfIPNetwork = new TextField();
			txfIPNetwork.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.UP) {
					getTxfIPMachine().requestFocus();
				} else if(value.getCode()==KeyCode.DOWN) {
					getTxfMask().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfIPNetwork;
	}

	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Retour");
			btnReturn.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnReturn.setOnAction((event)->{
				clear();
				this.setVisible(false);
				((RootSP)this.getParent()).getVbMenu().setVisible(true);
			});
		}
		return btnReturn;
	}

	public Button getBtnClear() {
		if(btnClear == null) {
			btnClear = new Button("Effacer");
			btnClear.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnClear.setOnAction((event)->{
				clear();
			});
		}
		return btnClear;
	}

	public Button getBtnValidate() {
		if(btnValidate == null) {
			btnValidate = new Button("Valider");
			btnValidate.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnValidate.setOnAction((event)->{
				try {
					getTxtAnswer().getStyleClass().remove("txt-error");
					getTxtAnswer().getStyleClass().add("txt-answer");
					if(NetworkCalculation.isInTheSameNetwork(getTxfIPMachine().getText(), getTxfIPNetwork().getText(), getTxfMask().getText())) {

						getTxtAnswer().setText("L'IP de la machine fait bien partie des IPs \n qui peuvent etre attribuees aux machines de ce reseau.");						
					} else {

						getTxtAnswer().setText("L'IP de la machine ne fait pas partie des IPs \n qui peuvent etre attribuees aux machines de ce reseau.");												

					}
				} catch (IpFormatException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Mauvais format pour une des IPs rentrees.");
				} catch (SikeThatsTheWrongNumberException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Nombre(s) trop(s) grand(s) ou trop(s) petit(s) pour une des IPs rentrees.");
				}
			});
		}
		return btnValidate;
	}

	public Text getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new Text("");
			txtAnswer.getStyleClass().add("txt-answer");
		}
		return txtAnswer;
	}
	
	public void clear() {
		getTxfIPMachine().clear();
		getTxfMask().clear();
		getTxfIPNetwork().clear();
		getTxtAnswer().setText("");
	}
}
