package view;

public interface IGraphicConstants {
	double NORMAL_SPACING = 35.;
	double NORMAL_PADDING = 35.;
	double MENU_BUTTON_WIDTH = 300.;
	double MENU_BUTTON_HEIGHT = 30.;
	double NORMAL_BUTTON_WIDTH = 125.;
	double NORMAL_BUTTON_HEIGHT = 30.;
}
