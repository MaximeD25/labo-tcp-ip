package view;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MenuVB extends VBox {
	private Text txtTitle;
	private Button btnClass;
	private Button btnNetwork;
	private Button btnBelong;
	private Button btnMachineNetwork;
	private Button btnCompareIP;
	private Button btnQuit;
	
	public MenuVB() {
		this.getChildren().addAll(getTxtTitle(), getBtnClass(), getBtnNetwork(), getBtnBelong(), getBtnMachineNetwork(), getBtnCompareIP(), getBtnQuit());
		this.setSpacing(25.);
		this.setAlignment(Pos.CENTER);
	}

	public Text getTxtTitle() {
		if(txtTitle == null) {
			txtTitle = new Text("Menu principal");
			txtTitle.getStyleClass().add("txt-menu-title");
		}
		return txtTitle;
	}

	public Button getBtnClass() {
		if(btnClass == null) {
			btnClass = new Button("Classe de l'IP");
			btnClass.setPrefSize(IGraphicConstants.MENU_BUTTON_WIDTH, IGraphicConstants.MENU_BUTTON_HEIGHT);
			btnClass.setOnAction((event)->{
				this.setVisible(false);
				((RootSP)this.getParent()).getVbFindClass().setVisible(true);
			});
		}
		return btnClass;
	}

	public Button getBtnNetwork() {
		if(btnNetwork == null) {
			btnNetwork = new Button("Information sur le reseau");
			btnNetwork.setPrefSize(IGraphicConstants.MENU_BUTTON_WIDTH, IGraphicConstants.MENU_BUTTON_HEIGHT);
			btnNetwork.setOnAction((event)->{
				this.setVisible(false);
				((RootSP)this.getParent()).getVbNetworkInformation().setVisible(true);
			});
		}
		return btnNetwork;
	}

	public Button getBtnBelong() {
		if(btnBelong == null) {
			btnBelong = new Button("IP appartenant au reseau");
			btnBelong.setPrefSize(IGraphicConstants.MENU_BUTTON_WIDTH, IGraphicConstants.MENU_BUTTON_HEIGHT);
			btnBelong.setOnAction((event)->{
				this.setVisible(false);
				((RootSP)this.getParent()).getVbBelongNetwork().setVisible(true);
			});
		}
		return btnBelong;
	}

	public Button getBtnMachineNetwork() {
		if(btnMachineNetwork == null) {
			btnMachineNetwork = new Button("Machine appartenant au reseau");
			btnMachineNetwork.setPrefSize(IGraphicConstants.MENU_BUTTON_WIDTH, IGraphicConstants.MENU_BUTTON_HEIGHT);
			btnMachineNetwork.setOnAction((event)->{
				this.setVisible(false);
				((RootSP)this.getParent()).getVbMachineNetwork().setVisible(true);
			});
		}
		return btnMachineNetwork;
	}

	public Button getBtnCompareIP() {
		if(btnCompareIP == null) {
			btnCompareIP = new Button("Comparaison de deux adresses IP");
			btnCompareIP.setPrefSize(IGraphicConstants.MENU_BUTTON_WIDTH, IGraphicConstants.MENU_BUTTON_HEIGHT);
			btnCompareIP.setOnAction((event)->{
				this.setVisible(false);
				((RootSP)this.getParent()).getVbCompareIP().setVisible(true);
			});
		}
		return btnCompareIP;
	}

	public Button getBtnQuit() {
		if(btnQuit == null) {
			btnQuit = new Button("Quitter");
			btnQuit.setPrefSize(IGraphicConstants.MENU_BUTTON_WIDTH, IGraphicConstants.MENU_BUTTON_HEIGHT);
			btnQuit.setOnAction((event)->{
				((Stage)this.getScene().getWindow()).close();
			});
		}
		return btnQuit;
	}
}
