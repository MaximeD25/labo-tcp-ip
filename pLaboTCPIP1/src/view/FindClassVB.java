package view;

import exception.IpFormatException;
import exception.SikeThatsTheWrongNumberException;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import util.NetworkCalculation;

public class FindClassVB extends VBox {
	private Text txtTitle;
	private GridPane gpInputs;
	private Label lblIP;
	private TextField txfIP;
	private Button btnReturn;
	private Button btnClear;
	private Button btnValidate;
	private Text txtAnswer;
	
	public FindClassVB() {
		//HBox pour les boutons
		HBox hbButton = new HBox(getBtnReturn(), getBtnClear(), getBtnValidate());
		hbButton.setAlignment(Pos.CENTER);
		hbButton.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		this.getChildren().addAll(getTxtTitle(), getGpInputs(), getTxtAnswer(), hbButton);
		this.setSpacing(IGraphicConstants.NORMAL_SPACING);
		this.setAlignment(Pos.CENTER);
	}

	public Text getTxtTitle() {
		if(txtTitle == null) {
			txtTitle = new Text("Determiner la classe d'une adresse IP");
			txtTitle.getStyleClass().add("txt-title");
		}
		return txtTitle;
	}
	
	public GridPane getGpInputs() {
		if(gpInputs == null) {
			gpInputs = new GridPane();
			//Defining the spacing and padding 
			gpInputs.setHgap(IGraphicConstants.NORMAL_SPACING);
			gpInputs.setVgap(IGraphicConstants.NORMAL_SPACING);
			gpInputs.setPadding(new Insets(IGraphicConstants.NORMAL_PADDING));
			//Defining the width of the column
			int nbCols = 2;
			for(int i=0; i<nbCols; i++) {
				ColumnConstraints constraint = new ColumnConstraints();
				constraint.setPercentWidth(100./nbCols);
				gpInputs.getColumnConstraints().add(constraint);
			}
			//Adding the elements to the table
			gpInputs.add(getLblIP(), 0, 0);
			gpInputs.add(getTxfIP(), 1, 0);
			//Aligning the elements 
			GridPane.setHalignment(getLblIP(), HPos.RIGHT);
			
		}
		return gpInputs;
	}

	public Label getLblIP() {
		if(lblIP == null) {
			lblIP = new Label("Adresse IP : ");
		}
		return lblIP;
	}

	public TextField getTxfIP() {
		if(txfIP == null) {
			txfIP = new TextField();
			txfIP.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfIP;
	}

	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Retour");
			btnReturn.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnReturn.setOnAction((event)->{
				clear();
				this.setVisible(false);
				((RootSP)this.getParent()).getVbMenu().setVisible(true);
			});
		}
		return btnReturn;
	}
	
	public Button getBtnClear() {
		if(btnClear == null) {
			btnClear = new Button("Effacer");
			btnClear.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnClear.setOnAction((event)->{
				clear();
			});
		}
		return btnClear;
	}

	public Button getBtnValidate() {
		if(btnValidate == null) {
			btnValidate = new Button("Valider");
			btnValidate.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnValidate.setOnAction((event)->{
				try {
					getTxtAnswer().getStyleClass().remove("txt-error");
					getTxtAnswer().getStyleClass().add("txt-answer");
					getTxtAnswer().setText("L'ip fait partie de la classe " + NetworkCalculation.getIpClass(getTxfIP().getText()));
				}  catch (IpFormatException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Mauvais format pour une des IPs rentrees.");
				} catch (SikeThatsTheWrongNumberException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Nombre(s) trop(s) grand(s) ou trop(s) petit(s) pour une des IPs rentrees.");
				}
				
			});
		}
		return btnValidate;
	}

	public Text getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new Text("");
			txtAnswer.getStyleClass().add("txt-answer");
		}
		return txtAnswer;
	}
	
	public void clear() {
		getTxfIP().clear();
		getTxtAnswer().setText("");
	}
}
