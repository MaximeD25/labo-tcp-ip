package view;

import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public class RootSP extends StackPane {
	private MenuVB vbMenu;
	private FindClassVB vbFindClass;
	private NetworkInformationVB vbNetworkInformation;
	private BelongNetworkVB vbBelongNetwork;
	private MachineNetworkVB vbMachineNetwork;
	private CompareIPVB vbCompareIP;
	
	public RootSP() {
		this.getChildren().addAll(getVbMenu(), getVbFindClass(), getVbNetworkInformation(), getVbBelongNetwork(), 
				getVbMachineNetwork(), getVbCompareIP());
		hideAll();
		getVbMenu().setVisible(true);
	}
	
	public MenuVB getVbMenu() {
		if(vbMenu == null) {
			vbMenu = new MenuVB();
		}
		return vbMenu;
	}
	
	public FindClassVB getVbFindClass() {
		if(vbFindClass == null) {
			vbFindClass = new FindClassVB();
		}
		return vbFindClass;
	}
	
	public NetworkInformationVB getVbNetworkInformation() {
		if(vbNetworkInformation == null) {
			vbNetworkInformation = new NetworkInformationVB();
		}
		return vbNetworkInformation;
	}
	
	public BelongNetworkVB getVbBelongNetwork() {
		if(vbBelongNetwork == null) {
			vbBelongNetwork = new BelongNetworkVB();
		}
		return vbBelongNetwork;
	}
	
	public MachineNetworkVB getVbMachineNetwork() {
		if(vbMachineNetwork == null) {
			vbMachineNetwork = new MachineNetworkVB();
		}
		return vbMachineNetwork;
	}
	
	public CompareIPVB getVbCompareIP() {
		if(vbCompareIP == null) {
			vbCompareIP = new CompareIPVB();
		}
		return vbCompareIP;
	}
	
	public void hideAll() {
		for(Node node : this.getChildren()) {
			node.setVisible(false);
		}
	}
}
