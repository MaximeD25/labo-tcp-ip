package view;

import exception.IpFormatException;
import exception.SikeThatsTheWrongNumberException;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import util.NetworkCalculation;

public class BelongNetworkVB extends VBox {
	private Text txtTitle;
	private GridPane gpInput;
	private Label lblIP;
	private TextField txfIP;
	private Label lblMask;
	private TextField txfMask;
	private Label lblNetwork;
	private TextField txfNetwork;
	private Button btnReturn;
	private Button btnClear;
	private Button btnValidate;
	private Text txtAnswer;
	
	public BelongNetworkVB() {
		//HBox pour les boutons
		HBox hbButtons = new HBox(getBtnReturn(), getBtnClear(), getBtnValidate());
		hbButtons.setAlignment(Pos.CENTER);
		hbButtons.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		this.getChildren().addAll(getTxtTitle(), getGpInput(), getTxtAnswer(), hbButtons);
		this.setSpacing(IGraphicConstants.NORMAL_SPACING);
		this.setAlignment(Pos.CENTER);
	}

	public Text getTxtTitle() {
		if(txtTitle == null) {
			txtTitle = new Text("IP appartenant au reseau");
			txtTitle.getStyleClass().add("txt-title");
		}
		return txtTitle;
	}
	
	public GridPane getGpInput() {
		if(gpInput == null) {
			gpInput = new GridPane();
			int nbCols = 2;
			//Defining the spacings
			gpInput.setHgap(IGraphicConstants.NORMAL_SPACING);
			gpInput.setVgap(IGraphicConstants.NORMAL_SPACING);
			gpInput.setPadding(new Insets(IGraphicConstants.NORMAL_PADDING));
			//Defining the width of the gridpane
			for(int i=0; i<nbCols; i++) {
				ColumnConstraints constraint = new ColumnConstraints();
				constraint.setPercentWidth(100./nbCols);
				gpInput.getColumnConstraints().add(constraint);
			}
			//Adding the elements to the gridpane
			gpInput.add(getLblIP(), 0, 0);
			gpInput.add(getTxfIP(), 1, 0);
			gpInput.add(getLblMask(), 0, 1);
			gpInput.add(getTxfMask(), 1, 1);
			gpInput.add(getLblNetwork(), 0, 2);
			gpInput.add(getTxfNetwork(), 1, 2);
			//Aligning the elements in the gridpane
			for(Node node : gpInput.getChildren()) {
				if(node instanceof Label) {
					GridPane.setHalignment(node, HPos.RIGHT);
				}
			}
		}
		return gpInput;
	}

	public Label getLblIP() {
		if(lblIP == null) {
			lblIP = new Label("Adresse IP : ");
		}
		return lblIP;
	}

	public TextField getTxfIP() {
		if(txfIP == null) {
			txfIP = new TextField();
			txfIP.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.DOWN) {
					getTxfMask().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfIP;
	}

	public Label getLblMask() {
		if(lblMask == null) {
			lblMask = new Label("Masque du reseau : ");
		}
		return lblMask;
	}

	public TextField getTxfMask() {
		if(txfMask == null) {
			txfMask = new TextField();
			txfMask.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.DOWN) {
					getTxfNetwork().requestFocus();
				} else if(value.getCode()==KeyCode.UP) {
					getTxfIP().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfMask;
	}

	public Label getLblNetwork() {
		if(lblNetwork == null) {
			lblNetwork = new Label("Adresse reseau : ");
		}
		return lblNetwork;
	}

	public TextField getTxfNetwork() {
		if(txfNetwork == null) {
			txfNetwork = new TextField();
			txfNetwork.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.UP) {
					getTxfMask().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfNetwork;
	}

	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Retour");
			btnReturn.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnReturn.setOnAction((event)->{
				clear();
				this.setVisible(false);
				((RootSP)this.getParent()).getVbMenu().setVisible(true);
			});
		}
		return btnReturn;
	}
	
	public Button getBtnClear() {
		if(btnClear == null) {
			btnClear = new Button("Effacer");
			btnClear.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnClear.setOnAction((event)->{
				clear();
			});
		}
		return btnClear;
	}

	public Button getBtnValidate() {
		if(btnValidate == null) {
			btnValidate = new Button("Valider");
			btnValidate.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnValidate.setOnAction((event)->{
				try {
					getTxtAnswer().getStyleClass().remove("txt-error");
					getTxtAnswer().getStyleClass().add("txt-answer");
					if(NetworkCalculation.compareIp(getTxfIP().getText(), getTxfNetwork().getText(), getTxfMask().getText())) {
						getTxtAnswer().setText("L'adresse IP appartient au reseau.");
					} else {
						getTxtAnswer().setText("L'adresse IP n'appartient pas au reseau.");
					}
				} catch (IpFormatException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Mauvais format pour une des IPs rentrees.");
				} catch (SikeThatsTheWrongNumberException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Nombre(s) trop(s) grand(s) ou trop(s) petit(s) pour une des IPs rentrees.");
				}
			});
		}
		return btnValidate;
	}

	public Text getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new Text("");
			txtAnswer.getStyleClass().add("txt-answer");
		}
		return txtAnswer;
	}
	
	public void clear() {
		getTxfIP().clear();
		getTxfMask().clear();
		getTxfNetwork().clear();
		getTxtAnswer().setText("");
	}
}
