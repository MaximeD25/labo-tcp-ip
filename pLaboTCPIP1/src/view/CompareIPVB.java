package view;

import exception.IpFormatException;
import exception.SikeThatsTheWrongNumberException;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import util.NetworkCalculation;

public class CompareIPVB extends VBox{
	private Text txtTitle;
	private GridPane gpInputs;
	private Label lblIP1;
	private TextField txfIP1;
	private Label lblMask1;
	private TextField txfMask1;
	private Label lblIP2;
	private TextField txfIP2;
	private Label lblMask2;
	private TextField txfMask2;
	private Button btnReturn;
	private Button btnClear;
	private Button btnValidate;
	private Text txtAnswer;
	
	public CompareIPVB() {
		//HBox pour l'adresse IP 1
		HBox hbIP1 = new HBox(getLblIP1(), getTxfIP1());
		hbIP1.setAlignment(Pos.CENTER);
		hbIP1.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		//HBox pour le masque de réseau 1
		HBox hbMask1 = new HBox(getLblMask1(), getTxfMask1());
		hbMask1.setAlignment(Pos.CENTER);
		hbMask1.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		//HBox pour l'adresse IP 2
		HBox hbIP2 = new HBox(getLblIP2(), getTxfIP2());
		hbIP2.setAlignment(Pos.CENTER);
		hbIP2.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		//HBox pour le masque du réseau 2
		HBox hbMask2 = new HBox(getLblMask2(), getTxfMask2());
		hbMask2.setAlignment(Pos.CENTER);
		hbMask2.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		//HBox pour les boutons
		HBox hbButtons = new HBox(getBtnReturn(), getBtnClear(), getBtnValidate());
		hbButtons.setAlignment(Pos.CENTER);
		hbButtons.setSpacing(IGraphicConstants.NORMAL_SPACING);
		
		this.getChildren().addAll(getTxtTitle(), getGpInputs(), getTxtAnswer(), hbButtons);
		this.setSpacing(IGraphicConstants.NORMAL_SPACING);
		this.setAlignment(Pos.CENTER);
	}

	public Text getTxtTitle() {
		if(txtTitle == null) {
			txtTitle = new Text("Comparaison de deux adresses IP");
			txtTitle.getStyleClass().add("txt-title");
		}
		return txtTitle;
	}
	
	public GridPane getGpInputs() {
		if(gpInputs == null) {
			gpInputs = new GridPane();
			//Defining the spacings and padding
			gpInputs.setHgap(IGraphicConstants.NORMAL_SPACING);
			gpInputs.setVgap(IGraphicConstants.NORMAL_SPACING);
			gpInputs.setPadding(new Insets(IGraphicConstants.NORMAL_PADDING));
			//Defining the width of the gridpane
			int nbCols = 2;
			for(int i=0; i<nbCols; i++) {
				ColumnConstraints constraint = new ColumnConstraints();
				constraint.setPercentWidth(100./nbCols);
				gpInputs.getColumnConstraints().add(constraint);
			}
			//Adding the elements to the gridpane
			gpInputs.add(getLblIP1(), 0, 0);
			gpInputs.add(getTxfIP1(), 1, 0);
			gpInputs.add(getLblIP2(), 0, 1);
			gpInputs.add(getTxfIP2(), 1, 1);
			gpInputs.add(getLblMask1(), 0, 2);
			gpInputs.add(getTxfMask1(), 1, 2);
			gpInputs.add(getLblMask2(), 0, 3);
			gpInputs.add(getTxfMask2(), 1, 3);
			//Aligning the elements
			for(Node node : gpInputs.getChildren()) {
				if(node instanceof Label) {
					GridPane.setHalignment(node, HPos.RIGHT);
				}
			}
		}
		return gpInputs;
	}

	public Label getLblIP1() {
		if(lblIP1 == null) {
			lblIP1 = new Label("Adresse IP 1 : ");
		}
		return lblIP1;
	}

	public TextField getTxfIP1() {
		if(txfIP1 == null) {
			txfIP1 = new TextField();
			txfIP1.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.DOWN) {
					getTxfIP2().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfIP1;
	}

	public Label getLblMask1() {
		if(lblMask1 == null) {
			lblMask1 = new Label("Masque du reseau 1 ");
		}
		return lblMask1;
	}

	public TextField getTxfMask1() {
		if(txfMask1 == null) {
			txfMask1 = new TextField();
			txfMask1.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.DOWN) {
					getTxfMask2().requestFocus();
				} else if(value.getCode()==KeyCode.UP) {
					getTxfIP2().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfMask1;
	}

	public Label getLblIP2() {
		if(lblIP2 == null) {
			lblIP2 = new Label("Adresse IP 2 : ");
		}
		return lblIP2;
	}

	public TextField getTxfIP2() {
		if(txfIP2 == null) {
			txfIP2 = new TextField();
			txfIP2.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.DOWN) {
					getTxfMask1().requestFocus();
				} else if(value.getCode()==KeyCode.UP) {
					getTxfIP1().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfIP2;
	}

	public Label getLblMask2() {
		if(lblMask2 == null) {
			lblMask2 = new Label("Masque du reseau 2 : ");
		}
		return lblMask2;
	}

	public TextField getTxfMask2() {
		if(txfMask2 == null) {
			txfMask2 = new TextField();
			txfMask2.setOnKeyPressed(value->{
				if(value.getCode()==KeyCode.UP) {
					getTxfMask1().requestFocus();
				} else if(value.getCode()==KeyCode.ENTER) {
					getBtnValidate().fire();
				}
			});
		}
		return txfMask2;
	}

	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Retour");
			btnReturn.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnReturn.setOnAction((event)->{
				clear();
				this.setVisible(false);
				((RootSP)this.getParent()).getVbMenu().setVisible(true);
			});
		}
		return btnReturn;
	}

	public Button getBtnClear() {
		if(btnClear == null) {
			btnClear = new Button("Effacer");
			btnClear.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnClear.setOnAction((event)->{
				clear();
			});
		}
		return btnClear;
	}

	public Button getBtnValidate() {
		if(btnValidate == null) {
			btnValidate = new Button("Valider");
			btnValidate.setPrefSize(IGraphicConstants.NORMAL_BUTTON_WIDTH, IGraphicConstants.NORMAL_BUTTON_HEIGHT);
			btnValidate.setOnAction((event)->{
				try {
					getTxtAnswer().getStyleClass().remove("txt-error");
					getTxtAnswer().getStyleClass().add("txt-answer");
					if(NetworkCalculation.compareIpWith2Mask(getTxfIP1().getText(), getTxfIP2().getText(), getTxfMask1().getText(), getTxfMask2().getText())) {
						getTxtAnswer().setText("Les machines font parties du meme reseau.");						
					} else {
						getTxtAnswer().setText("Les machines ne font pas partie du meme reseau.");
					}
				} catch (IpFormatException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Mauvais format pour une des IPs rentrees.");
				} catch (SikeThatsTheWrongNumberException e) {
					getTxtAnswer().getStyleClass().add("txt-error");
					getTxtAnswer().setText("Nombre(s) trop(s) grand(s) ou trop(s) petit(s) pour une des IPs rentrees.");
				}
			});
		}
		return btnValidate;
	}

	public Text getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new Text("");
			txtAnswer.getStyleClass().add("txt-answer");
		}
		return txtAnswer;
	}
	
	public void clear() {
		getTxfIP1().clear();
		getTxfIP2().clear();
		getTxfMask1().clear();
		getTxfMask2().clear();
		getTxtAnswer().setText("");
	}
}
