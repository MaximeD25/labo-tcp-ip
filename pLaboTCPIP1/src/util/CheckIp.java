package util;

import exception.IpFormatException;
import exception.SikeThatsTheWrongNumberException;

public class CheckIp {

	public static boolean checkIp(String ip) throws IpFormatException, SikeThatsTheWrongNumberException {
		//Check the size of the ip
		if(ip==null || ip.length()<7 || ip.length()>15) throw new IpFormatException();
		
		//Split the ip
		String[] partsIp = {"", "", "", ""};
		int index = 0;
		for(int i=0; i<ip.length(); i++) {
			if(ip.substring(i, i+1).equals(".")) {
				index++;
				continue;
			}
			if(index==4) {
				throw new IpFormatException();
			}
			partsIp[index] += ip.substring(i, i+1);
		}
		
		//Check the numbers of the ip
		for(int i=0; i<partsIp.length; i++) {
			int number;
			try {
				number = Integer.parseInt(partsIp[i]);
			} catch (NumberFormatException e) {
				throw new IpFormatException();
			}
			
			//Check if the numbers are between 0 and 255
			if(number<0 || number>255) throw new SikeThatsTheWrongNumberException();
		}
	
		return true;
	}
}
