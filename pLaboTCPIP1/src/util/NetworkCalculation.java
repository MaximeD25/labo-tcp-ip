package util;

import exception.IpFormatException;
import exception.SikeThatsTheWrongNumberException;

public class NetworkCalculation {
	/**
	 * 
	 * @param ipAdress An IP adress in the format xxx.xxx.xxx.xxx
	 * @return A table of Strings in which each element is a byte of the IP adress in binary
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static String[] convertToBinary(String ipAdress) throws IpFormatException, SikeThatsTheWrongNumberException {
		//Split the string into the four parts of the ip adress
		String[] partsIp = splitStringIp(ipAdress);
		//Transform those strings into binary
		String[] ipBinary = new String[4];
		for(int i=0; i<partsIp.length; i++) {
			ipBinary[i] = Integer.toBinaryString(Integer.parseInt(partsIp[i]));
			//Adding zero in front for later
			while(ipBinary[i].length() < 8) {
				ipBinary[i] = "0" + ipBinary[i].substring(0, ipBinary[i].length());
			}
		}
		return ipBinary;
	}
	
	/**
	 * @param mask The mask of the network in the format xxx.xxx.xxx.xxx
	 * @param ipAdress An IP adress in the format xxx.xxx.xxx.xxx
	 * @return A table of Strings in which each element is a byte of the IP adress in binary
	 * with the mask applied on the IP adress
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static String[] applyMaskOnIp(String mask, String ipAdress) throws IpFormatException, SikeThatsTheWrongNumberException {
		String[] partsMask = convertToBinary(mask);
		String[] partsIp = convertToBinary(ipAdress);
		for(int i=0; i<4; i++) {
			char[] partIp = partsIp[i].toCharArray();
			for(int j=0; j<8; j++) {
				if(partsMask[i].substring(j, j+1).equals("0")) {
					partIp[j] = '0';
				}
			}
			partsIp[i] = String.valueOf(partIp);
		}
		return partsIp;
	}
	
	/**
	 * @param ipAdress1 An IP adress in the format xxx.xxx.xxx.xxx
	 * @param ipAdress2 An IP adress in the format xxx.xxx.xxx.xxx
	 * @param mask The mask of the network in the format xxx.xxx.xxx.xxx
	 * @return True if both ip adresses belongs to the same network or false otherwise
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static boolean compareIp(String ipAdress1, String ipAdress2, String mask) throws IpFormatException, SikeThatsTheWrongNumberException {
		String[] ip1 = applyMaskOnIp(mask, ipAdress1);
		String[] ip2 = applyMaskOnIp(mask, ipAdress2);
		for(int i=0; i<4; i++) {
			for(int j=0; j<8; j++) {
				if(!ip1[i].substring(j, j+1).equals(ip2[i].substring(j, j+1))) {
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * @param ipAdress An IP adress in the format xxx.xxx.xxx.xxx
	 * @return A table of Strings where each element is a byte of the IP adress in decimal
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	private static String[] splitStringIp(String ipAdress) throws IpFormatException, SikeThatsTheWrongNumberException {
		//Check
		CheckIp.checkIp(ipAdress);
		//Split the ipAdress at each dot (Problem with method split)
		String[] partsIp = {"", "", "", ""};
		int index = 0;
		for(int i=0; i<ipAdress.length(); i++) {
			if(ipAdress.substring(i, i+1).equals(".")) {
				index++;
				continue;
			}
			partsIp[index] += ipAdress.substring(i, i+1);
		}
		return partsIp;
	}
	
	/**
	 * @param ipAdress An IP adress in the format xxx.xxx.xxx.xxx
	 * @return The letter of the class of the IP 
	 * @throws NumberFormatException
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static String getIpClass(String ipAdress) throws NumberFormatException, IpFormatException, SikeThatsTheWrongNumberException {
		int ips = Integer.parseInt(splitStringIp(ipAdress)[0]);
		
		if(ips >= 0 && ips < 127) {
			return "A";
		} else if(ips < 192) {
			return "B";
		} else if(ips < 224) {
			return "C";
		} else if(ips < 240) {
			return "D";
		} else if (ips < 256) {
			return "E";
		}
		return null;
	}
	
	/**
	 * @param mask The mask of the network in the format xxx.xxx.xxx.xxx
	 * @param ipAdress An IP adress in the format xxx.xxx.xxx.xxx
	 * @return A table of Strings where each element represent a byte of the broadcast adress
	 * in binary
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static String[] calculBroadcastIp(String mask, String ipAdress) throws IpFormatException, SikeThatsTheWrongNumberException {
		//Calcul the broadcast ip with an ip and a mask
        String[] partsMask = convertToBinary(mask);
        String[] partsIp = convertToBinary(ipAdress);
        for(int i=0; i<4; i++) {
			char[] partIp = partsIp[i].toCharArray();
			for(int j=0; j<8; j++) {
				if(partsMask[i].substring(j, j+1).equals("0")) {
					partIp[j] = '1';
				}
			}
			partsIp[i] = String.valueOf(partIp);
		}
		return partsIp;
    }
	
	/**
	 * @param ipAdress An IP adress in the format xxx.xxx.xxx.xxx
	 * @return A string representing the mask of the class of the ip in decimal
	 * @throws NumberFormatException
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static String getIpMaskClass(String ipAdress) throws NumberFormatException, IpFormatException, SikeThatsTheWrongNumberException {
		//Return the mask of the class of an ip
		int ips = Integer.parseInt(splitStringIp(ipAdress)[0]);
		if(ips >= 0 && ips < 127) {
			 return "255.0.0.0";
		} else if(ips < 192) {
			return "255.255.0.0";
		} else if(ips < 224) {
			return "255.255.255.0";
		} else if(ips < 240) {
			return "255.255.255.255";
		}
		return null;
	}
	
	/**
	 * @param netMaskIp The mask of the network in the format xxx.xxx.xxx.xxx
	 * @param classMaskIp The mask of the network in the format xxx.xxx.xxx.xxx
	 * @return True if the masks are the same and false otherwise
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static boolean compare2Mask(String netMaskIp, String classMaskIp) throws IpFormatException, SikeThatsTheWrongNumberException {
		//Check
		CheckIp.checkIp(netMaskIp);
		CheckIp.checkIp(classMaskIp);		
		//Return true if 2 masks are equals
		return netMaskIp.equals(classMaskIp);
	}
	
	/**
	 * 
	 * @param ipAdress
	 * @param netAdress
	 * @param netMask
	 * @return
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static boolean isInTheSameNetwork(String ipAdress, String netAdress, String netMask) throws IpFormatException, SikeThatsTheWrongNumberException {
		if(!compareIp(ipAdress, netAdress, netMask) 
				|| ipAdress.equals(netAdress)
				|| ipAdress.equals(convertToDecimal(calculBroadcastIp(netMask, ipAdress)))) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param ipAdress1 An IP adress in the format xxx.xxx.xxx.xxx
	 * @param ipAdress2 An IP adress in the format xxx.xxx.xxx.xxx
	 * @param maskIp1 The mask of the network in the format xxx.xxx.xxx.xxx
	 * @param maskIp2 The mask of the network in the format xxx.xxx.xxx.xxx
	 * @return True if both ips belongs to the same network
	 * @throws IpFormatException
	 * @throws SikeThatsTheWrongNumberException
	 */
	public static boolean compareIpWith2Mask(String ipAdress1, String ipAdress2, String maskIp1, String maskIp2) throws IpFormatException, SikeThatsTheWrongNumberException {
		String[] ip1 = applyMaskOnIp(maskIp1, ipAdress1);
		String[] ip2 = applyMaskOnIp(maskIp2, ipAdress2);
		for(int i=0; i<4; i++) {
			for(int j=0; j<8; j++) {
				if(!ip1[i].substring(j, j+1).equals(ip2[i].substring(j, j+1))) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param partsIp A table of Strings where each element represents a byte of the IP adress
	 * in binary
	 * @return A String that represents th IP adress in decimal
	 */
	public static String convertToDecimal(String[] partsIp) {
		int[] somme = {0,0,0,0};
		for(int i=0; i<4; i++) {
			for(int j=0; j<8; j++) {
				somme[i] += Integer.parseInt(partsIp[i].substring(j,j+1)) * Math.pow(2, 7-j);
			}
		}
		return somme[0]+"."+somme[1]+"."+somme[2]+"."+somme[3];
	}
}
